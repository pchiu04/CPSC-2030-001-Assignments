<?php
//twig
require_once "sqlhelper.php";
require_once './vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

//sql
$user = 'CPSC2030';
$pwd = 'CPSC2030';
$server = 'localhost';
$dbname = 'pokedex';
$conn = new mysqli($server, $user, $pwd, $dbname);

$name = mysqli_real_escape_string($conn, $_GET["pokemon"]);
$result = $conn->query("call pokemon_info(\"$name\")");
clearConnection($conn);

if($result){
    $table = $result->fetch_all(MYSQLI_ASSOC);
    $detailpage = $twig->load('detailpage.twig.html');

    $strong = $conn->query("call strong_Against()");
    clearConnection($conn);
    $weak = $conn->query("call weak_Against()");
    clearConnection($conn);
    $resistant = $conn->query("call resistant_To()");
    clearConnection($conn);
    $vulnerable = $conn->query("call vulnerable_To()");
    clearConnection($conn);

    if ($strong && $weak && $resistant && $vulnerable) {
        $strongAgainst = $strong->fetch_all(MYSQLI_ASSOC);
        $weakAgainst = $weak->fetch_all(MYSQLI_ASSOC);
        $resistantTo = $resistant->fetch_all(MYSQLI_ASSOC);
        $vulnerableTo = $vulnerable->fetch_all(MYSQLI_ASSOC);
    }

    foreach($table as $row){
        echo $detailpage->render(array('pokemon_info'=>$row, 'strongAgainst'=>$strongAgainst,
    'weakAgainst'=>$weakAgainst, 'resistantTo'=>$resistantTo, 'vulnerableTo'=>$vulnerableTo));
    }

    $conn->close();
} else {
    echo "error";
}
?>
