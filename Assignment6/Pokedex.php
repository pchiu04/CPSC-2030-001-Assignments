<?php
session_start();
//twig
require_once "sqlhelper.php";
require_once './vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

//sql setup
$user = 'CPSC2030';
$pwd = 'CPSC2030';
$server = 'localhost';
$dbname = 'pokedex';
$conn = new mysqli($server, $user, $pwd, $dbname);
$result = $conn->query("call pokemon_list()");
clearConnection($conn);
$allType = $conn->query("call all_type_list()");
clearConnection($conn);
$top10 = $conn->query("call top10()");
clearConnection($conn);

if (!$allType && !$top10Pokemon){
    echo "all type error";
}

if($result){
    $table = $result->fetch_all(MYSQLI_ASSOC);
}

if (isset($_GET['type'])){ //if there's a type already call
    $type = mysqli_real_escape_string($conn, $_GET["type"]); //escape the type string
    $newPokedex = $conn->query("call pokemon_type_list(\"$type\")");  //grab a new pokedex that contains this type string
    clearConnection($conn);
    if ($newPokedex){
        $table = $newPokedex->fetch_all(MYSQLI_ASSOC);
    } else {
        echo "Error: call pokemon_type_list(Type)";
    }
}



if (isset($_GET['favourite'])){ //if there's a pokemon added to favourite
    $fav = mysqli_real_escape_string($conn, $_GET["favourite"]);

    if (isset($_SESSION['favourite'][$fav])){
        unset ($_SESSION['favourite'][$fav]);
        $_SESSION['counter']--;
    } else if ($_SESSION['counter'] < 7){
        $_SESSION['favourite'][$fav] = $fav;
        $_SESSION['counter']++;
    }
}

if (isset($_GET['remove'])){ //if there's a pokemon added to favourite
    $remove = mysqli_real_escape_string($conn, $_GET["remove"]);
    unset ($_SESSION['favourite'][$remove]);
    $_SESSION['counter']--;
}

$pokedex = $twig->load('main.twig.html');
echo $pokedex->render(array("pokemon_list"=>$table, "type_list"=>$allType,
 "top10"=>$top10, "session"=>$_SESSION['favourite'])); //pokemon table



//session_destroy();
?>
