DELIMITER //
CREATE PROCEDURE pokemon_info (IN string text)
BEGIN
    SELECT *
    FROM pokemon
    where pokemon.Name = string;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE pokemon_list ()
BEGIN
    SELECT National, Name, Type
    FROM pokemon;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE pokemon_type_list ()
BEGIN
SELECT *
FROM pokemon
where pokemon.Type LIKE type OR
pokemon.Type LIKE concat('% ', type) OR
pokemon.Type LIKE concat('% ', type, ' %') OR
pokemon.Type LIKE concat(type, ' %');
END
DELIMITER ;


DELIMITER //
CREATE PROCEDURE strong_Against ()
BEGIN
  SELECT *
  From strongagainst;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE vulnerable_To ()
BEGIN
  SELECT *
  From vulnerableTo;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE resistant_To ()
BEGIN
  SELECT *
  From resistantTo;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE weak_Against ()
BEGIN
  SELECT *
  From weakAgainst;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE pokemon_type (IN string text)
BEGIN
    SELECT Type
    FROM pokemon
    where pokemon.Name = string;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE all_type_list ()
BEGIN
    SELECT * FROM alltype;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE pokemon_type_list (IN type text)
BEGIN
    SELECT * FROM pokemon
    where pokemon.Type = type;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE top10 ()
BEGIN
    SELECT * FROM top10;
END //
DELIMITER ;
