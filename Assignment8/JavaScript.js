const selectedClass = "selected";

//grab the class .menu
let allItems = $(".menu");

//add a click event to the class .menuButton
$(".menuButton").click(function (event) {
    //add a toggle class ".selected" to .menu
    allItems.toggleClass(selectedClass);
})
