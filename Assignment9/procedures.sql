DELIMITER //
CREATE PROCEDURE getMessageByTime (IN string text)
BEGIN
    SELECT TimeOfContent
    FROM messageData
    where messageData.TimeOfContent = string;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE getRecentMessage ()
BEGIN
    SELECT *
    FROM messageData
    LIMIT 10;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE storeMessage (IN nameM text, IN timeM text, IN messageM text)
BEGIN
    INSERT INTO messageData VALUES (nameM, timeM, messageM);
END //
DELIMITER ;
