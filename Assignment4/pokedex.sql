CREATE TABLE pokemon(
    National int,
    Hoenn text,
    Name text,
    Type text,
    HP int,
    Atk int,
    Def int,
    SAt int,
    SDf int,
    Spd int,
    BST int
);

CREATE TABLE strongAgainst(
    Normal text,
    Fighting text,
    Flying text,
    Posion text,
    Ground text,
    Rock text,
    Bug text,
    Ghost text,
    Steel text,
    Fire text,
    Water text,
    Grass text,
    Electric text,
    Psychic text,
    Ice text,
    Dragon text,
    Fairy text,
    Dark text
);

CREATE TABLE weakAgainst(
    Normal text,
    Fighting text,
    Flying text,
    Posion text,
    Ground text,
    Rock text,
    Bug text,
    Ghost text,
    Steel text,
    Fire text,
    Water text,
    Grass text,
    Electric text,
    Psychic text,
    Ice text,
    Dragon text,
    Fairy text,
    Dark text
);

CREATE TABLE resistantTo(
    Normal text,
    Fighting text,
    Flying text,
    Posion text,
    Ground text,
    Rock text,
    Bug text,
    Ghost text,
    Steel text,
    Fire text,
    Water text,
    Grass text,
    Electric text,
    Psychic text,
    Ice text,
    Dragon text,
    Fairy text,
    Dark text
);

CREATE TABLE vulnerableTo(
    Normal text,
    Fighting text,
    Flying text,
    Posion text,
    Ground text,
    Rock text,
    Bug text,
    Ghost text,
    Steel text,
    Fire text,
    Water text,
    Grass text,
    Electric text,
    Psychic text,
    Ice text,
    Dragon text,
    Fairy text,
    Dark text
);

INSERT INTO pokemon VALUES (387, "-", "Turtwig", "Grass", 55, 68, 64, 45, 55, 31, 318);
INSERT INTO pokemon VALUES (388, "-", "Grotle", "Grass", 75, 89, 85, 55, 65, 36, 405);
INSERT INTO pokemon VALUES (389, "-", "Torterra", "Grass Ground", 95, 109, 105, 75, 85, 56, 525);
INSERT INTO pokemon VALUES (390, "-", "Chimchar", "Fire", 44, 58, 44, 58, 44, 61, 309);
INSERT INTO pokemon VALUES (391, "-", "Monferno", "Fire Fighting", 64, 78, 52, 78, 52, 81, 405);
INSERT INTO pokemon VALUES (392, "-", "Infernape", "Fire Fighting", 76, 104, 71, 104, 71, 108, 534);
INSERT INTO pokemon VALUES (393, "-", "Piplup", "Water", 53, 51, 53, 61, 56, 40, 314);
INSERT INTO pokemon VALUES (394, "-", "Prinplup", "Water", 64, 66, 68, 81, 76, 50, 405);
INSERT INTO pokemon VALUES (395, "-", "Empoleon", "Water Steel", 84, 86, 88, 111, 101, 60, 530);
INSERT INTO pokemon VALUES (396, "-", "Starly", "Normal Flying", 40, 55, 30, 30, 30, 60, 245);
INSERT INTO pokemon VALUES (397, "-", "Staravia", "Normal Flying", 55, 75, 50, 40, 40, 80, 340);
INSERT INTO pokemon VALUES (398, "-", "Staraptor", "Normal Flying", 85, 120, 70, 50, 60, 100, 485);
INSERT INTO pokemon VALUES (399, "-", "Bidoof", "Normal", 59, 45, 40, 35, 40, 31, 250);
INSERT INTO pokemon VALUES (400, "-", "Bibarel", "Normal Water", 79, 85, 60, 55, 60, 71, 410);
INSERT INTO pokemon VALUES (401, "-", "Kricketot", "Bug", 37, 25, 41, 25, 41, 25, 194);
INSERT INTO pokemon VALUES (402, "-", "Kricketune", "Bug", 77, 85, 51, 55, 51, 65, 384);
INSERT INTO pokemon VALUES (403, "-", "Shinx", "Electric", 45, 65, 34, 40, 34, 45, 263);
INSERT INTO pokemon VALUES (404, "-", "Luxio", "Electric", 60, 85, 49, 60, 49, 60, 363);
INSERT INTO pokemon VALUES (405, "-", "Luxray", "Electric", 80, 120, 79, 95, 79, 70, 523);
INSERT INTO pokemon VALUES (406, "97", "Budew", "Grass Poison", 40, 30, 35, 50, 70, 55, 280);
INSERT INTO pokemon VALUES (407, "99", "Roserade", "Grass Poison", 60, 70, 65, 125, 105, 90, 515);
INSERT INTO pokemon VALUES (408, "-", "Cranidos", "Rock", 67, 125, 40, 30, 30, 58, 350);
INSERT INTO pokemon VALUES (409, "-","Rampardos", "Rock", 97, 165, 60, 65, 50, 58, 495);
INSERT INTO pokemon VALUES (410, "-", "Shieldon", "Rock Steel", 30, 42, 118, 42, 88, 30, 350);
INSERT INTO pokemon VALUES (411, "-", "Bastiodon", "Rock Steel", 60, 52, 168, 47, 138, 30, 495);
INSERT INTO pokemon VALUES (412, "-", "Burmy", "Bug", 40, 29, 45, 29, 45, 36, 224);
INSERT INTO pokemon VALUES (413, "-", "Wormadam", "Bug Grass", 60, 59, 85, 79, 105, 36, 424);
INSERT INTO pokemon VALUES (413, "-", "Wormadam(Sandy Cloak)", "Bug Ground", 60, 79, 105, 59, 85, 36, 424);
INSERT INTO pokemon VALUES (413, "-", "Wormadam(Trash Cloak)", "Bug Steel", 60, 69, 95, 69, 95, 36, 424);
INSERT INTO pokemon VALUES (414, "-", "Mothim", "Bug Flying", 70, 94, 50, 94, 50, 66, 424);
INSERT INTO pokemon VALUES (415, "-", "Combee", "Bug Flying", 30, 30, 42, 30, 42, 70, 244);
INSERT INTO pokemon VALUES (416, "-", "Vespiquen", "Bug Flying", 70, 80, 102, 80, 102, 40, 474);
INSERT INTO pokemon VALUES (417, "-", "Pachirisu", "Electric", 60, 45, 70, 45, 90, 95, 405);
INSERT INTO pokemon VALUES (418, "-", "Buizel", "Water", 55, 65, 35, 60, 30, 85, 330);
INSERT INTO pokemon VALUES (419, "-", "Floatzel", "Water", 85, 105, 55, 85, 50, 115, 495);
INSERT INTO pokemon VALUES (420, "-", "Cherubi", "Grass", 45, 35, 45, 62, 53, 35, 275);
INSERT INTO pokemon VALUES (421, "-", "Cherrim", "Grass", 70, 60, 70, 87, 78, 85, 450);
INSERT INTO pokemon VALUES (421, "-", "Cherrim (Sunshine Form)", "Grass", 70, 60, 70, 87, 78, 85, 450);
INSERT INTO pokemon VALUES (422, "-", "Shellos", "Water", 76, 48, 48, 57, 62, 34, 325);
INSERT INTO pokemon VALUES (423, "-", "Gastrodon", "Water Ground", 111, 83, 68, 92, 82, 39, 475);
INSERT INTO pokemon VALUES (424, "-", "Ambipom", "Normal", 75, 100, 66, 60, 66, 115, 482);
INSERT INTO pokemon VALUES (425, "-", "Drifloon", "Ghost Flying", 90, 50, 34, 60, 44, 70, 348);
INSERT INTO pokemon VALUES (426, "-", "Drifblim", "Ghost Flying", 150, 80, 44, 90, 54, 80, 498);
INSERT INTO pokemon VALUES (427, "-", "Buneary", "Normal", 55, 66, 44, 44, 56, 85, 350);
INSERT INTO pokemon VALUES (428, "-", "Lopunny", "Normal", 65, 76, 84, 54, 96, 105, 480);
INSERT INTO pokemon VALUES (428, "-", "Mega Lopunny", "Normal Fighting", 65, 136, 94, 54, 96, 135, 580);
INSERT INTO pokemon VALUES (429, "-", "Mismagius", "Ghost", 60, 60, 60, 105, 105, 105, 495);
INSERT INTO pokemon VALUES (430, "-", "Honchkrow", "Dark Flying", 100, 125, 52, 105, 52, 71, 505);
INSERT INTO pokemon VALUES (431, "-", "Glameow", "Normal", 49, 55, 42, 42, 37, 85, 310);
INSERT INTO pokemon VALUES (432, "-", "Purugly", "Normal", 71, 82, 64, 64, 59, 112, 452);
INSERT INTO pokemon VALUES (433, "157", "Chingling", "Psychic", 45, 30, 50, 65, 50, 45, 285);
INSERT INTO pokemon VALUES (434, "-", "Stunky", "Poison Dark", 63, 63, 47, 41, 41, 74, 329);
INSERT INTO pokemon VALUES (435, "-", "Skuntank", "Poison Dark", 103, 93, 67, 71, 61, 84, 479);
INSERT INTO pokemon VALUES (436, "-", "Bronzor", "Steel Psychic", 57, 24, 86, 24, 86, 23, 300);
INSERT INTO pokemon VALUES (437, "-", "Bronzong", "Steel Psychic", 67, 89, 116, 79, 116, 33, 500);
INSERT INTO pokemon VALUES (438, "-", "Bonsly", "Rock", 50, 80, 95, 10, 45, 10, 290);
INSERT INTO pokemon VALUES (439, "-", "Mime Jr.", "Psychic Fairy", 20, 25, 45, 70, 90, 60, 310);
INSERT INTO pokemon VALUES (440, "-", "Happiny", "Normal", 100, 5, 5, 15, 65, 30, 220);
INSERT INTO pokemon VALUES (441, "-", "Chatot", "Normal Flying", 76, 65, 45, 92, 42, 91, 411);
INSERT INTO pokemon VALUES (442, "-", "Spiritomb", "Ghost Dark", 50, 92, 108, 92, 108, 35, 485);
INSERT INTO pokemon VALUES (443, "-", "Gible", "Dragon Ground", 58, 70, 45, 40, 45, 42, 300);
INSERT INTO pokemon VALUES (444, "-", "Gabite", "Dragon Ground", 68, 90, 65, 50, 55, 82, 410);
INSERT INTO pokemon VALUES (445, "-", "Garchomp", "Dragon Ground", 108, 130, 95, 80, 85, 102, 600);
INSERT INTO pokemon VALUES (445, "-", "Mega Garchomp", "Dragon Ground", 108, 170, 115, 120, 95, 92, 700);
INSERT INTO pokemon VALUES (446, "-", "Munchlax", "Normal", 135, 85, 40, 40, 85, 5, 390);
INSERT INTO pokemon VALUES (447, "-", "Riolu", "Fighting", 40, 70, 40, 35, 40, 60, 285);
INSERT INTO pokemon VALUES (448, "-", "Lucario", "Fighting Steel", 70, 110, 70, 115, 70, 90, 525);
INSERT INTO pokemon VALUES (448, "-", "Mega Lucario", "Fighting Steel", 70, 145, 88, 140, 70, 112, 625);
INSERT INTO pokemon VALUES (449, "-", "Hippopotas", "Ground", 68, 72, 78, 38, 42, 32, 330);
INSERT INTO pokemon VALUES (450, "-", "Hippowdon", "Ground", 108, 112, 118, 68, 72, 47, 525);
INSERT INTO pokemon VALUES (451, "-", "Skorupi", "Poison Bug", 40, 50, 90, 30, 55, 65, 330);
INSERT INTO pokemon VALUES (452, "-", "Drapion", "Poison Dark", 70, 90, 110, 60, 75, 95, 500);
INSERT INTO pokemon VALUES (453, "-", "Croagunk", "Poison Fighting", 48, 61, 40, 61, 40, 50, 300);
INSERT INTO pokemon VALUES (454, "-", "Toxicroak", "Poison Fighting", 83, 106, 65, 86, 65, 85, 490);
INSERT INTO pokemon VALUES (455, "-", "Carnivine", "Grass", 74, 100, 72, 90, 72, 46, 454);
INSERT INTO pokemon VALUES (456, "-", "Finneon", "Water", 49, 49, 56, 49, 61, 66, 330);
INSERT INTO pokemon VALUES (457, "-", "Lumineon", "Water", 69, 69, 76, 69, 86, 91, 460);
INSERT INTO pokemon VALUES (458, "-", "Mantyke", "Water Flying", 45, 20, 50, 60, 120, 50, 345);
INSERT INTO pokemon VALUES (459, "-", "Snover", "Grass Ice", 60, 62, 50, 62, 60, 40, 334);
INSERT INTO pokemon VALUES (460, "-", "Abomasnow", "Grass Ice", 90, 92, 75, 92, 85, 60, 494);
INSERT INTO pokemon VALUES (460, "-", "Mega Abomasnow", "Grass Ice", 90, 132, 105, 132, 105, 30, 594);
INSERT INTO pokemon VALUES (461, "-", "Weavile", "Dark Ice", 70, 120, 65, 45, 85, 125, 510);
INSERT INTO pokemon VALUES (462, "86", "Magnezone", "Electric Steel", 70, 70, 115, 130, 90, 60, 535);
INSERT INTO pokemon VALUES (463, "-", "Lickilicky", "Normal", 110, 85, 95, 80, 95, 50, 515);
INSERT INTO pokemon VALUES (464, "178", "Rhyperior", "Ground Rock", 115, 140, 130, 55, 55, 40, 535);
INSERT INTO pokemon VALUES (465, "-", "Tangrowth", "Grass", 100, 100, 125, 110, 50, 50, 535);
INSERT INTO pokemon VALUES (466, "-", "Electivire", "Electric", 75, 123, 67, 95, 85, 95, 540);
INSERT INTO pokemon VALUES (467, "-", "Magmortar", "Fire", 75, 95, 67, 125, 95, 83, 540);
INSERT INTO pokemon VALUES (468, "-", "Togekiss", "Fairy Flying", 85, 50, 95, 120, 115, 80, 545);
INSERT INTO pokemon VALUES (469, "-", "Yanmega", "Bug Flying", 86, 76, 86, 116, 56, 95, 515);
INSERT INTO pokemon VALUES (470, "-", "Leafeon", "Grass", 65, 110, 130, 60, 65, 95, 525);
INSERT INTO pokemon VALUES (471, "-", "Glaceon", "Ice", 65, 60, 110, 130, 95, 65, 525);
INSERT INTO pokemon VALUES (472, "-", "Gliscor", "Ground Flying", 75, 95, 125, 45, 75, 95, 510);
INSERT INTO pokemon VALUES (473, "-", "Mamoswine", "Ice Ground", 110, 130, 80, 70, 60, 80, 530);
INSERT INTO pokemon VALUES (474, "-", "Porygon-Z", "Normal", 85, 80, 70, 135, 75, 90, 535);
INSERT INTO pokemon VALUES (475, "32", "Gallade", "Psychic Fighting", 68, 125, 65, 65, 115, 80, 518);
INSERT INTO pokemon VALUES (475, "32", "Mega Gallade", "Psychic Fighting", 68, 165, 95, 65, 115, 110, 618);
INSERT INTO pokemon VALUES (476, "62", "Probopass", "Rock Steel", 60, 55, 145, 75, 150, 40, 525);
INSERT INTO pokemon VALUES (477, "155", "Dusknoir", "Ghost", 45, 100, 135, 65, 135, 45, 525);
INSERT INTO pokemon VALUES (478, "181", "Froslass", "Ice Ghost", 70, 80, 70, 80, 70, 110, 480);
INSERT INTO pokemon VALUES (479, "-", "Rotom", "Electric Ghost", 50, 50, 77, 95, 77, 91, 440);
INSERT INTO pokemon VALUES (479, "-", "Rotom (Heat Rotom)", "Electric Fire", 50, 65, 107, 105, 107, 86, 520);
INSERT INTO pokemon VALUES (479, "-", "Rotom (Wash Rotom)", "Electric Water", 50, 65, 107, 105, 107, 86, 520);
INSERT INTO pokemon VALUES (479, "-", "Rotom (Frost Rotom)", "Electric Ice", 50, 65, 107, 105, 107, 86, 520);
INSERT INTO pokemon VALUES (479, "-", "Rotom (Fan Rotom)", "Electric Flying", 50, 65, 107, 105, 107, 86, 520);
INSERT INTO pokemon VALUES (479, "-", "Rotom (Mow Rotom)", "Electric Grass", 50, 65, 107, 105, 107, 86, 520);
INSERT INTO pokemon VALUES (480, "-", "Uxie", "Psychic", 75, 75, 130, 75, 130, 95, 580);
INSERT INTO pokemon VALUES (481, "-", "Mesprit", "Psychic", 80, 105, 105, 105, 105, 80, 580);
INSERT INTO pokemon VALUES (482, "-", "Azelf", "Psychic", 75, 125, 70, 125, 70, 115, 580);
INSERT INTO pokemon VALUES (483, "-", "Dialga", "Steel Dragon", 100, 120, 120, 150, 100, 90, 680);
INSERT INTO pokemon VALUES (484, "-", "Palkia", "Water Dragon", 90, 120, 100, 150, 120, 100, 680);
INSERT INTO pokemon VALUES (485, "-", "Heatran", "Fire Steel", 91, 90, 106, 130, 106, 77, 600);
INSERT INTO pokemon VALUES (486, "-", "Regigigas", "Normal", 110, 160, 110, 80, 110, 100, 670);
INSERT INTO pokemon VALUES (487, "-", "Giratina", "Ghost Dragon", 150, 100, 120, 100, 120, 90, 680);
INSERT INTO pokemon VALUES (487, "-", "Giratina (Origin Forme)", "Ghost Dragon", 150, 120, 100, 120, 100, 90, 680);
INSERT INTO pokemon VALUES (488, "-", "Cresselia", "Psychic", 120, 70, 120, 75, 130, 85, 600);
INSERT INTO pokemon VALUES (489, "-", "Phione", "Water", 80, 80, 80, 80, 80, 80, 480);
INSERT INTO pokemon VALUES (490, "-", "Manaphy", "Water", 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES (491, "-", "Darkrai", "Dark", 70, 90, 90, 135, 90, 125, 600);
INSERT INTO pokemon VALUES (492, "-", "Shaymin", "Grass", 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES (492, "-", "Shaymin (Sky Forme)", "Grass Flying", 100, 103, 75, 120, 75, 127, 600);
INSERT INTO pokemon VALUES (493, "-", "Arceus", "Normal", 120, 120, 120, 120, 120, 120, 720);
INSERT INTO pokemon VALUES (494, "-", "Victini", "Psychic Fire", 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES (495, "-", "Snivy", "Grass", 45, 45, 55, 45, 55, 63, 308);
INSERT INTO pokemon VALUES (496, "-", "Servine", "Grass", 60, 60, 75, 60, 75, 83, 413);
INSERT INTO pokemon VALUES (497, "-", "Serperior", "Grass", 75, 75, 95, 75, 95, 113, 528);
INSERT INTO pokemon VALUES (498, "-", "Tepig", "Fire", 65, 63, 45, 45, 45, 45, 308);
INSERT INTO pokemon VALUES (499, "-", "Pignite", "Fire Fighting", 90, 93, 55, 70, 55, 55, 418);
INSERT INTO pokemon VALUES (500, "-", "Emboar", "Fire Fighting", 110, 123, 65, 100, 65, 65, 528);
INSERT INTO pokemon VALUES (501, "-", "Oshawott", "Water", 55, 55, 45, 63, 45, 45, 308);
INSERT INTO pokemon VALUES (502, "-", "Dewott", "Water", 75, 75, 60, 83, 60, 60, 413);
INSERT INTO pokemon VALUES (503, "-", "Samurott", "Water", 95, 100, 85, 108, 70, 70, 528);
INSERT INTO pokemon VALUES (504, "-", "Patrat", "Normal", 45, 55, 39, 35, 39, 42, 255);
INSERT INTO pokemon VALUES (505, "-", "Watchog", "Normal", 60, 85, 69, 60, 69, 77, 420);
INSERT INTO pokemon VALUES (506, "-", "Lillipup", "Normal", 45, 60, 45, 25, 45, 55, 275);
INSERT INTO pokemon VALUES (507, "-", "Herdier", "Normal", 65, 80, 65, 35, 65, 60, 370);
INSERT INTO pokemon VALUES (508, "-", "Stoutland", "Normal", 85, 110, 90, 45, 90, 80, 500);
INSERT INTO pokemon VALUES (509, "-", "Purrloin", "Dark", 41, 50, 37, 50, 37, 66, 281);
INSERT INTO pokemon VALUES (510, "-", "Liepard", "Dark", 64, 88, 50, 88, 50, 106, 446);
INSERT INTO pokemon VALUES (511, "-", "Pansage", "Grass", 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES (512, "-", "Simisage", "Grass", 75, 98, 63, 98, 63 ,101, 498);
INSERT INTO pokemon VALUES (513, "-", "Pansear", "Fire", 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES (514, "-", "Simisear", "Fire", 75, 98, 63, 98, 63, 101, 498);
INSERT INTO pokemon VALUES (515, "-", "Panpour", "Water", 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES (516, "-", "Simipour", "Water", 75, 98, 63, 98, 63, 101, 498);
INSERT INTO pokemon VALUES (517, "-", "Munna", "Psychic", 76, 25, 45, 67, 55, 24, 292);
INSERT INTO pokemon VALUES (518, "-", "Musharna", "Psychic", 116, 55, 85, 107, 95, 29, 487);
INSERT INTO pokemon VALUES (519, "-", "Pidove", "Normal Flying", 50, 55, 50, 36, 30, 43, 264);
INSERT INTO pokemon VALUES (520, "-", "Tranquill", "Normal Flying", 62, 77, 62, 50, 42, 65, 358);
INSERT INTO pokemon VALUES (521, "-", "Unfezant", "Normal Flying", 80, 115, 80, 65, 55, 93, 488);
INSERT INTO pokemon VALUES (522, "-", "Blitzle", "Electric", 45, 60, 32, 50, 32, 76, 295);
INSERT INTO pokemon VALUES (523, "-", "Zebstrika", "Electric", 75, 100, 63, 80, 63, 116, 497);
INSERT INTO pokemon VALUES (524, "-", "Roggenrola", "Rock", 55, 75, 85, 25, 25, 15, 280);
INSERT INTO pokemon VALUES (525, "-", "Boldore", "Rock", 70, 105, 105, 50, 40, 20, 390);
INSERT INTO pokemon VALUES (526, "-", "Gigalith", "Rock", 85, 135, 130, 60, 80, 25, 515);
INSERT INTO pokemon VALUES (527, "-", "Woobat", "Psychic Flying", 55, 45, 43, 55, 43, 72, 313);
INSERT INTO pokemon VALUES (528, "-", "Swoobat", "Psychic Flying", 67, 57, 55, 77, 55, 114, 425);
INSERT INTO pokemon VALUES (529, "-", "Drilbur", "Ground", 60, 85, 40, 30, 45, 68, 328);
INSERT INTO pokemon VALUES (530, "-", "Excadrill", "Ground Steel", 110, 135, 60, 50, 65, 88, 508);
INSERT INTO pokemon VALUES (531, "-", "Audino", "Normal", 103, 60, 86, 60, 86, 50, 445);
INSERT INTO pokemon VALUES (531, "-", "Mega Audino", "Normal Fairy", 103, 60, 126, 80, 126, 50, 545);
INSERT INTO pokemon VALUES (532, "-", "Timburr", "Fighting", 75, 80, 55, 25, 35, 35, 305);
INSERT INTO pokemon VALUES (533, "-", "Gurdurr", "Fighting", 85, 105, 85, 40, 50, 40, 405);
INSERT INTO pokemon VALUES (534, "-", "Conkeldurr", "Fighting", 105, 140, 95, 55, 65, 45, 505);
INSERT INTO pokemon VALUES (535, "-", "Tympole", "Water", 50, 50, 40, 50, 40, 64, 294);
INSERT INTO pokemon VALUES (536, "-", "Palpitoad", "Water Ground", 75, 65, 55, 65, 55, 69, 384);
INSERT INTO pokemon VALUES (537, "-", "Seismitoad", "Water Ground", 105, 95, 75, 85, 75, 74, 509);
INSERT INTO pokemon VALUES (538, "-", "Throh", "Fighting", 120, 100, 85, 30, 85, 45, 465);
INSERT INTO pokemon VALUES (539, "-", "Sawk", "Fighting", 75, 125, 75, 30, 75, 85, 465);
INSERT INTO pokemon VALUES (540, "-", "Sewaddle", "Bug Grass", 45, 53, 70, 40, 60, 42, 310);
INSERT INTO pokemon VALUES (541, "-", "Swadloon", "Bug Grass", 55 ,63 ,90 ,50 ,80 ,42 ,380);
INSERT INTO pokemon VALUES (542, "-", "Leavanny", "Bug Grass", 75, 103, 80, 70, 80, 92, 500);
INSERT INTO pokemon VALUES (543, "-", "Venipede", "Bug Poison", 30, 45, 59, 30, 39, 57, 260);
INSERT INTO pokemon VALUES (544, "-", "Whirlipede", "Bug Poison", 40, 55, 99, 40, 79, 47, 360);
INSERT INTO pokemon VALUES (545, "-", "Scolipede", "Bug Poison", 60, 100, 89, 55, 69, 112, 485);
INSERT INTO pokemon VALUES (546, "-", "Cottonee", "Grass Fairy", 40, 27, 60, 37, 50, 66, 280);
INSERT INTO pokemon VALUES (547, "-", "Whimsicott", "Grass Fairy", 60, 67, 85, 77, 75, 116, 480);
INSERT INTO pokemon VALUES (548, "-", "Petilil", "Grass", 45, 35, 50, 70, 50, 30, 280);
INSERT INTO pokemon VALUES (549, "-", "Lilligant", "Grass", 70, 60, 75, 110, 75, 90, 480);
INSERT INTO pokemon VALUES (550, "-", "Basculin", "Water", 70, 92, 65, 80, 55, 98, 460);
INSERT INTO pokemon VALUES (550, "-", "Basculin (Blue-Striped Form)", "Water", 70, 92, 65, 80, 55, 98, 460);
INSERT INTO pokemon VALUES (551, "-", "Sandile", "Ground Dark", 50, 72, 35, 35, 35, 65, 292);
INSERT INTO pokemon VALUES (552, "-", "Krokorok", "Ground Dark", 60, 82, 45, 45, 45, 74, 351);
INSERT INTO pokemon VALUES (553, "-", "Krookodile", "Ground Dark", 95, 117, 80, 65, 70, 92, 519);
INSERT INTO pokemon VALUES (554, "-", "Darumaka", "Fire", 70, 90, 45, 15, 45, 50, 315);
INSERT INTO pokemon VALUES (555, "-", "Darmanitan", "Fire", 105, 140, 55, 30, 55, 95, 480);
INSERT INTO pokemon VALUES (555, "-", "Darmanitan (Zen Mode)", "Fire Psychic", 105, 30, 105, 140, 105, 55, 540);
INSERT INTO pokemon VALUES (556, "-", "Maractus", "Grass", 75, 86, 67, 106, 67, 60, 461);
INSERT INTO pokemon VALUES (557, "-", "Dwebble", "Bug Rock", 50, 65, 85, 35, 35, 55, 325);
INSERT INTO pokemon VALUES (558, "-", "Crustle", "Bug Rock", 70, 95, 125, 65, 75, 45, 475);
INSERT INTO pokemon VALUES (559, "-", "Scraggy", "Dark Fighting", 50, 75, 70, 35, 70, 48, 348);
INSERT INTO pokemon VALUES (560, "-", "Scrafty", "Dark Fighting", 65, 90, 115, 45, 115, 58, 488);
INSERT INTO pokemon VALUES (561, "-", "Sigilyph", "Psychic Flying", 72, 58, 80, 103, 80, 97, 490);
INSERT INTO pokemon VALUES (562, "-", "Yamask", "Ghost", 38, 30, 85, 55, 65, 30, 303);
INSERT INTO pokemon VALUES (563, "-", "Cofagrigus", "Ghost", 58, 50, 145, 95, 105, 30, 483);
INSERT INTO pokemon VALUES (564, "-", "Tirtouga", "Water Rock", 54, 78, 103, 53, 45, 22, 355);
INSERT INTO pokemon VALUES (565, "-", "Carracosta", "Water Rock", 74, 108, 133, 83, 65, 32, 495);
INSERT INTO pokemon VALUES (566, "-", "Archen", "Rock Flying", 55, 112, 45, 74, 45, 70, 401);
INSERT INTO pokemon VALUES (567, "-", "Archeops", "Rock Flying", 75, 140, 65, 112, 65, 110, 567);
INSERT INTO pokemon VALUES (568, "-", "Trubbish", "Poison", 50, 50, 62, 40, 62, 65, 329);
INSERT INTO pokemon VALUES (569, "-", "Garbodor", "Poison", 80, 95, 82, 60, 82, 75, 474);
INSERT INTO pokemon VALUES (570, "-", "Zorua", "Dark", 40, 65, 40, 80, 40, 65, 330);
INSERT INTO pokemon VALUES (571, "-", "Zoroark", "Dark", 60, 105, 60, 120, 60, 105, 510);
INSERT INTO pokemon VALUES (572, "-", "Minccino", "Normal", 55, 50, 40, 40, 40, 75, 300);
INSERT INTO pokemon VALUES (573, "-", "Cinccino", "Normal", 75, 95, 60, 65, 60, 115, 470);
INSERT INTO pokemon VALUES (574, "-", "Gothita", "Psychic", 45, 30, 50, 55, 65, 45, 290);
INSERT INTO pokemon VALUES (575, "-", "Gothorita", "Psychic", 60, 45, 70, 75, 85, 55, 390);
INSERT INTO pokemon VALUES (576, "-", "Gothitelle", "Psychic", 70, 55, 95, 95, 110, 65, 490);
INSERT INTO pokemon VALUES (577, "-", "Solosis", "Psychic", 45, 30, 40, 105, 50, 20, 290);
INSERT INTO pokemon VALUES (578, "-",  "Duosion", "Psychic", 65, 40, 50, 125, 60, 30, 370);
INSERT INTO pokemon VALUES (579, "-", "Reuniclus", "Psychic", 110, 65, 75, 125, 85, 30, 490);
INSERT INTO pokemon VALUES (580, "-", "Ducklett", "Water Flying", 62, 44, 50, 44, 50, 55, 305);
INSERT INTO pokemon VALUES (581, "-", "Swanna", "Water Flying", 75, 87, 63, 87, 63, 98, 473);
INSERT INTO pokemon VALUES (582, "-", "Vanillite", "Ice", 36, 50, 50, 65, 60, 44, 305);
INSERT INTO pokemon VALUES (583, "-", "Vanillish", "Ice", 51, 65, 65, 80, 75, 59, 395);
INSERT INTO pokemon VALUES (584, "-", "Vanilluxe", "Ice", 71, 95, 85, 110, 95, 79, 535);
INSERT INTO pokemon VALUES (585, "-", "Deerling", "Normal Grass", 60, 60, 50, 40, 50, 75, 335);
INSERT INTO pokemon VALUES (586, "-", "Sawsbuck", "Normal Grass", 80, 100, 70, 60, 70, 95, 475);
INSERT INTO pokemon VALUES (587, "-", "Emolga", "Electric Flying", 55, 75, 60, 75, 60, 103, 428);
INSERT INTO pokemon VALUES (588, "-", "Karrablast", "Bug", 50, 75, 45, 40, 45, 60, 315);
INSERT INTO pokemon VALUES (589, "-", "Escavalier", "Bug Steel", 70, 135, 105, 60, 105, 20, 495);
INSERT INTO pokemon VALUES (590, "-", "Foongus", "Grass Poison", 69, 55, 45, 55, 55, 15, 294);
INSERT INTO pokemon VALUES (591, "-", "Amoonguss", "Grass Poison", 114, 85, 70, 85, 80, 30, 464);
INSERT INTO pokemon VALUES (592, "-", "Frillish", "Water Ghost", 55, 40, 50, 65, 85, 40, 335);
INSERT INTO pokemon VALUES (593, "-", "Jellicent", "Water Ghost", 100, 60, 70, 85, 105, 60, 480);
INSERT INTO pokemon VALUES (594, "-", "Alomomola", "Water", 165, 75, 80, 40, 45, 65, 470);
INSERT INTO pokemon VALUES (595, "-", "Joltik", "Bug Electric", 50, 47, 50, 57, 50, 65, 319);
INSERT INTO pokemon VALUES (596, "-", "Galvantula", "Bug Electric", 70, 77, 60, 97, 60, 108, 472);
INSERT INTO pokemon VALUES (597, "-", "Ferroseed", "Grass Steel", 44, 50, 91, 24, 86, 10, 305);
INSERT INTO pokemon VALUES (598, "-", "Ferrothorn", "Grass Steel", 74, 94, 131, 54, 116, 20, 489);
INSERT INTO pokemon VALUES (599, "-", "Klink", "Steel", 40, 55, 70, 45, 60, 30, 300);
INSERT INTO pokemon VALUES (600, "-", "Klang", "Steel", 60, 80, 95, 70, 85, 50, 440);
INSERT INTO pokemon VALUES (601, "-", "Klinklang", "Steel", 60, 100, 115, 70, 85, 90, 520);
INSERT INTO pokemon VALUES (602, "-", "Tynamo", "Electric", 35, 55, 40, 45, 40, 60, 275);
INSERT INTO pokemon VALUES (603, "-", "Eelektrik", "Electric", 65, 85, 70, 75, 70, 40, 405);
INSERT INTO pokemon VALUES (604, "-", "Eelektross", "Electric", 85, 115, 80, 105, 80, 50, 515);
INSERT INTO pokemon VALUES (605, "-", "Elgyem", "Psychic", 55, 55, 55, 85, 55, 30, 335);
INSERT INTO pokemon VALUES (606, "-", "Beheeyem", "Psychic", 75, 75, 75, 125, 95, 40, 485);
INSERT INTO pokemon VALUES (607, "-", "Litwick", "Ghost Fire", 50, 30, 55, 65, 55, 20, 275);
INSERT INTO pokemon VALUES (608, "-", "Lampent", "Ghost Fire", 60, 40, 60, 95, 60, 55, 370);
INSERT INTO pokemon VALUES (609, "-", "Chandelure", "Ghost Fire", 60, 55, 90, 145, 90, 80, 520);
INSERT INTO pokemon VALUES (610, "-", "Axew", "Dragon", 46, 87, 60, 30, 40, 57, 320);
INSERT INTO pokemon VALUES (611, "-", "Fraxure", "Dragon", 66, 117, 70, 40, 50, 67, 410);
INSERT INTO pokemon VALUES (612, "-", "Haxorus", "Dragon", 76, 147, 90, 60, 70, 97, 540);
INSERT INTO pokemon VALUES (613, "-", "Cubchoo", "Ice", 55, 70, 40, 60, 40, 40, 305);
INSERT INTO pokemon VALUES (614, "-", "Beartic", "Ice", 95, 110, 80, 70, 80, 50, 485);
INSERT INTO pokemon VALUES (615, "-", "Cryogonal", "Ice", 70, 50, 30, 95, 135, 105, 485);
INSERT INTO pokemon VALUES (616, "-", "Shelmet", "Bug", 50, 40, 85, 40, 65, 25, 305);
INSERT INTO pokemon VALUES (617, "-", "Accelgor", "Bug", 80, 70, 40, 100, 60, 145, 495);
INSERT INTO pokemon VALUES (618, "-", "Stunfisk", "Ground Electric", 109, 66, 84, 81, 99, 32, 471);
INSERT INTO pokemon VALUES (619, "-", "Mienfoo", "Fighting", 45, 85, 50, 55, 50, 65, 350);
INSERT INTO pokemon VALUES (620, "-", "Mienshao", "Fighting", 65, 125, 60, 95, 60, 105, 510);
INSERT INTO pokemon VALUES (621, "-", "Druddigon", "Dragon", 77, 120, 90, 60, 90, 48, 485);
INSERT INTO pokemon VALUES (622, "-", "Golett", "Ground Ghost", 59, 74, 50, 35, 50, 35, 303);
INSERT INTO pokemon VALUES (623, "-", "Golurk", "Ground Ghost", 89, 124, 80, 55, 80, 55, 483);
INSERT INTO pokemon VALUES (624, "-", "Pawniard", "Dark Steel", 45, 85, 70, 40, 40, 60, 340);
INSERT INTO pokemon VALUES (625, "-", "Bisharp", "Dark Steel", 65, 125, 100, 60, 70, 70, 490);
INSERT INTO pokemon VALUES (626, "-", "Bouffalant", "Normal", 95, 110, 95, 40, 95, 55, 490);
INSERT INTO pokemon VALUES (627, "-", "Rufflet", "Normal Flying", 70, 83, 50, 37, 50, 60, 350);

INSERT INTO strongAgainst VALUES ("", "Normal", "Fighting", "Grass", "Poison", "Flying", "Grass", "Ghost", "Rock", "Bug", "Ground", "Ground", "Flying", "Fighting", "Flying", "Dragon", "Fighting", "Ghost");
INSERT INTO strongAgainst VALUES ("", "Rock", "Bug", "Fairy", "Rock", "Bug", "Psychic", "Psychic", "Ice", "Steel", "Rock", "Rock", "Water", "Poison", "Ground", "", "Dragon", "Psychic");
INSERT INTO strongAgainst VALUES ("", "Steel", "Grass", "", "Steel", "Fire", "Dark", "", "Fairy", "Grass", "Fire", "Water", "", "", "Grass", "", "Dark", "");
INSERT INTO strongAgainst VALUES ("", "Ice", "", "", "Fire", "Ice", "", "", "", "Ice", "", "", "", "", "Dragon", "", "", "");
INSERT INTO strongAgainst VALUES ("", "Dark", "", "", "Electric", "", "", "", "", "", "", "", "", "", "", "", "", "");

INSERT INTO weakAgainst VALUES ("Rock", "Flying", "Rock", "Poison", "Flying", "Fighting", "Fighting", "Normal", "Steel", "Rock", "Water", "Flying", "Ground", "Steel", "Steel", "Steel", "Poison", "Fighting");
INSERT INTO weakAgainst VALUES ("Ghost", "Poison", "Steel", "Ground", "Bug", "Ground", "Flying", "Dark", "Fire", "Fire", "Grass", "Poison", "Grass", "Psychic", "Fire", "Fairy", "Steel", "Dark");
INSERT INTO weakAgainst VALUES ("Steel", "Psychic", "Electric", "Rock", "Grass", "Steel", "Poison", "", "Water", "Water", "Dragon", "Bug", "Electric", "Dark", "Water", "", "Fire", "Fairy");
INSERT INTO weakAgainst VALUES ("", "Bug", "", "Ghost", "", "", "Ghost", "", "Electric", "Dragon", "", "Steel", "Dragon", "", "Ice", "", "", "");
INSERT INTO weakAgainst VALUES ("", "Ghost", "", "Steel", "", "", "Steel", "", "", "", "", "Fire", "", "", "", "", "", "");
INSERT INTO weakAgainst VALUES ("", "Fairy", "", "", "", "", "Fire", "", "", "", "", "Grass", "", "", "", "", "", "");
INSERT INTO weakAgainst VALUES ("", "", "", "", "", "", "Fairy", "", "", "", "", "Dragon", "", "", "", "", "", "");

INSERT INTO resistantTo VALUES ("Ghost", "Rock", "Fighting", "Fighting", "Poison", "Normal", "Fighting", "Normal", "Normal", "Bug", "Steel", "Ground", "Flying", "Fighting", "Ice", "Fire", "Fighting", "Ghost");
INSERT INTO resistantTo VALUES ("", "Bug", "Ground", "Poison", "Rock", "Flying", "Ground", "Fighting", "Flying", "Steel", "Fire", "Water", "Steel", "Psychic", "", "Water", "Bug", "Psychic");
INSERT INTO resistantTo VALUES ("", "Dark", "Bug", "Grass", "Electric", "Poison", "Grass", "Poison", "Poison", "Fire", "Water", "Grass", "Electric", "", "", "Grass", "Dragon", "Dark");
INSERT INTO resistantTo VALUES ("", "", "Grass", "Fairy", "", "Fire", "", "Bug", "Rock", "Grass", "Ice", "Electric", "", "", "", "Electric", "Dark", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Bug", "Ice", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Steel", "", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Grass", "", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Psychic", "", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Ice", "", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Dragon", "", "", "", "", "", "", "", "", "");
INSERT INTO resistantTo VALUES ("", "", "", "", "", "", "", "", "Fairy", "", "", "", "", "", "", "", "", "");

INSERT INTO vulnerableTo VALUES ("Fighting", "Flying", "Rock", "Ground", "Water", "Fighting", "Flying", "Ghost", "Fighting", "Ground", "Grass", "Flying", "Ground", "Bug", "Fighting", "Ice", "Poison", "Fighting");
INSERT INTO vulnerableTo VALUES ("", "Psychic", "Electric", "Psychic", "Grass", "Ground", "Rock", "Dark", "Ground", "Rock", "Electric", "Poison", "", "Ghost", "Rock", "Dragon", "Steel", "Bug");
INSERT INTO vulnerableTo VALUES ("", "Fairy", "Ice", "", "Ice", "Steel", "Fire", "", "Fire", "Water", "", "Bug", "", "Dark", "Steel", "Fairy", "", "Fairy");
INSERT INTO vulnerableTo VALUES ("", "", "", "", "", "Water", "", "", "", "", "", "Fire", "", "", "Fire", "", "", "");
INSERT INTO vulnerableTo VALUES ("", "", "", "", "", "Grass", "", "", "", "", "", "Ice", "", "", "", "", "", "");
