SELECT DISTINCT National, Name, Type FROM pokemon
INNER JOIN strongagainst ON pokemon.Type LIKE concat('% ', strongagainst.Ground, ' %') OR
pokemon.Type LIKE concat('% ', strongagainst.Ground) OR
pokemon.Type LIKE concat(strongagainst.Ground, ' %')
INNER JOIN weakagainst ON pokemon.Type LIKE concat('% ', weakagainst.Steel, ' %')  OR
pokemon.Type LIKE concat('% ', weakagainst.Steel) OR
pokemon.Type LIKE concat(weakagainst.Steel, ' %');
