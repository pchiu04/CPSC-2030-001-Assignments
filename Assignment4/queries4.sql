SELECT DISTINCT National, Name, Type, Atk
FROM pokemon
INNER JOIN strongagainst ON pokemon.Type LIKE concat('% ', strongagainst.Fire, ' %') OR
pokemon.Type LIKE concat('% ', strongagainst.Fire) OR
pokemon.Type LIKE concat(strongagainst.Fire, ' %')
WHERE Name LIKE 'Mega%'
ORDER BY Atk DESC LIMIT 1
