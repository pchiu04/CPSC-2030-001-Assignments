SELECT DISTINCT National, Name, Type, BST FROM pokemon
INNER JOIN resistantTo ON pokemon.Type LIKE concat('% ', resistantTo.Water, ' %') OR
pokemon.Type LIKE concat('% ', resistantTo.Water) OR
pokemon.Type LIKE concat(resistantTo.Water, ' %')
WHERE BST BETWEEN 200 AND 500;
