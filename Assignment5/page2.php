<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Document</title>
</head>
<?php
include "sqlhelper.php";
$user = 'CPSC2030';
$pwd = 'CPSC2030';
$server = 'localhost';
$dbname = 'pokedex';

$conn = new mysqli($server, $user, $pwd, $dbname);

$name = mysqli_real_escape_string($conn, $_GET["pokemon"]); //prevent SQL injection
$result = $conn->query("call pokemon_info(\"$name\")");  //Need to escape quotes inside quotes
clearConnection($conn);
?>
<body>
    <?php
    if($result){
        echo "<div class=title>".$name."</div>"; //the pokemon name
        $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays

        foreach($table as $row){
            echo "<div class=detail>";
            echo "<div class=national>National #".$row["National"]."</div>";
            echo "<div class=type>Type: ".$row["Type"]."</div>";
            echo "HP:<div class=hp>".$row["HP"]."</div>";
            echo "Attack:<div class=atk>".$row["Atk"]."</div>";
            echo "Defense:<div class=def>".$row["Def"]."</div>";
            echo "Special Attack:<div class=sat>".$row["SAt"]."</div>";
            echo "Special Defense:<div class=sdf>".$row["SDf"]."</div>";
            echo "Speed:<div class=spd>".$row["Spd"]."</div>";
            echo "Base Stats Total:<div class=bst>".$row["BST"]."</div>";
            echo "</div>";
            $typeString = $row["Type"]; //get the pokemon type
            $typeArray = explode(" ", $typeString); //split types into it's own arrays

            echo "<div class=typeEffectiveness>";
            echo "<div class=strongAgainst>";
            echo "<div class=typeTitle>Strong Against</div>";
            $strongAgainst = $conn->query("call strong_Against()"); //get the effectiveness from query
            if ($strongAgainst) {
                $strong = $strongAgainst->fetch_all(MYSQLI_ASSOC);
                foreach ($typeArray as $i){ //compare pokemon's type(1, 2, 3..) with the effect type's correct column
                    foreach($strong as $row){
                        if ($row["$i"] != ''){ //only echo the ones that's not empty inside the effectiveAgainst[pokemonType]
                            echo $row["$i"]." ";
                        }
                    }
                }
            } else {
                echo "error";
            }
            clearConnection($conn);
            echo "</div>";

            //repeat for 3 more times for other effects
            echo "<div class=weakAgainst>";
            echo "<div class=typeTitle>Weak Against</div>";
            $weakAgainst = $conn->query("call weak_Against()");
            if ($weakAgainst) {
                $weak = $weakAgainst->fetch_all(MYSQLI_ASSOC);
                foreach ($typeArray as $i){
                    foreach($weak as $row){
                        if ($row["$i"] != ''){
                            echo $row["$i"]." ";
                        }
                    }
                }
            } else {
                echo "error";
            }
            clearConnection($conn);
            echo "</div>";

            echo "<div class=resistantTo>";
            echo "<div class=typeTitle>Resistant To</div>";
            $resistantTo = $conn->query("call resistant_To()");
            if ($resistantTo) {
                $resist= $resistantTo->fetch_all(MYSQLI_ASSOC);
                foreach ($typeArray as $i){
                    foreach($resist as $row){
                        if ($row["$i"] != ''){
                            echo $row["$i"]." ";
                        }
                    }
                }

            } else {
                echo "error";
            }
            clearConnection($conn);
            echo "</div>";

            echo "<div class=vulnerableTo>";
            echo "<div class=typeTitle>Vulnerable To</div>";
            $vulerableTo = $conn->query("call vulnerable_To()");
            if ($vulerableTo) {
                $vulerable = $vulerableTo->fetch_all(MYSQLI_ASSOC);
                foreach($typeArray as $i){
                    foreach($vulerable as $row){
                        if ($row["$i"] != ''){
                            echo $row["$i"]." ";
                        }
                    }
                }
            } else {
                echo "error";
            }
            clearConnection($conn);
            echo "</div>";
            echo "</div>";
        }

    } else {
        echo "error";
    }
    ?>
</body>
</html>
