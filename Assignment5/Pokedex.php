<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Pokedex</title>
</head>
<?php
include "sqlhelper.php";
$user = 'CPSC2030';
$pwd = 'CPSC2030';
$server = 'localhost';
$dbname = 'pokedex';

$conn = new mysqli($server, $user, $pwd, $dbname);
$result = $conn->query("call pokemon_list()");
clearConnection($conn);
?>

<body>
    <div class="title">Choose a Pokemon!</div>
    <?php
    if($result){
        $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays
        echo "<div class=typeLinks>";
        echo "<div class=normal><a href=Pokedex.php?type=Normal>Normal</a></div>";
        echo "<div class=fighting><a href=Pokedex.php?type=Fighting>Fighting</a></div>";
        echo "<div class=flying><a href=Pokedex.php?type=Flying>Flying</a></div>";
        echo "<div class=poison><a href=Pokedex.php?type=Poison>Poison</a></div>";
        echo "<div class=ground><a href=Pokedex.php?type=Ground>Ground</a></div>";
        echo "<div class=rock><a href=Pokedex.php?type=Rock>Rock</a></div>";
        echo "<div class=bug><a href=Pokedex.php?type=Bug>Bug</a></div>";
        echo "<div class=ghost><a href=Pokedex.php?type=Ghost>Ghost</a></div>";
        echo "<div class=steel><a href=Pokedex.php?type=Steel>Steel</a></div>";
        echo "<div class=fire><a href=Pokedex.php?type=Fire>Fire</a></div>";
        echo "<div class=water><a href=Pokedex.php?type=Water>Water</a></div>";
        echo "<div class=grass><a href=Pokedex.php?type=Grass>Grass</a></div>";
        echo "<div class=electric><a href=Pokedex.php?type=Electric>Electric</a></div>";
        echo "<div class=psychic><a href=Pokedex.php?type=Psychic>Psychic</a></div>";
        echo "<div class=ice><a href=Pokedex.php?type=Ice>Ice</a></div>";
        echo "<div class=dragon><a href=Pokedex.php?type=Dragon>Dragon</a></div>";
        echo "<div class=fairy><a href=Pokedex.php?type=Fairy>Fairy</a></div>";
        echo "<div class=dark><a href=Pokedex.php?type=Dark>Dark</a></div>";
        echo "</div>";

        echo "<div class=content>";
        if (isset($_GET['type'])){ //checks if there is a type selected
            foreach($table as $row){
                $typeArray = explode(" ", $row["Type"]); //breaks pokemon types into array for multi-comparison
                foreach($typeArray as $typeRow){
                    if ($typeRow == $_GET['type']){ //only echo the pokemon with the correct type
                        $link = "page2.php?pokemon=".urlencode($row["Name"]);
                        echo "<div class=pokemon_list>";
                        echo "<div class=national>".$row["National"]."</div>&nbsp";
                        echo "<a href= '$link'>".$row["Name"]."</a>&nbsp-&nbsp";
                        echo "<div class=type>".$row["Type"]."</div>";
                        echo "</div>";
                    }
                }

            }
        } else { //if no type selected, just echo all pokemon
            foreach($table as $row){
                $link = "page2.php?pokemon=".urlencode($row["Name"]);
                echo "<div class=pokemon_list>";
                echo "<div class=national>".$row["National"]."</div>&nbsp";
                echo "<a href= '$link'>".$row["Name"]."</a>&nbsp-&nbsp";
                echo "<div class=type>".$row["Type"]."</div>";
                echo "</div>";
            }
        }
        echo "</div>";

    } else {
        echo "Error";
    }
    ?>
</body>
</html>
