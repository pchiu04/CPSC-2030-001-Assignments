CREATE DATABASE forms;
CREATE TABLE forms (
    Name text,
    Email text,
    Location text,
    PhoneNumber int,
    Messsage text
);


DELIMITER //
CREATE PROCEDURE insert_info (IN name text, IN email text, IN location text, IN phone int, IN message text)
BEGIN
    INSERT INTO forms VALUES (name, email, location, phone, message);
END //
DELIMITER ;
