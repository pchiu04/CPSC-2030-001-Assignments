function setupSendButton(){
    $("#sendButton").click(function (event) {
        $.ajax({
            method: "POST",
            url: "project.php",
            data: {
                user: $("#username").val(),
                email: $("#email").val(),
                location: $("#location").val(),
                phone: $("#phone").val(),
                message: $("#message").val()
            }
        }).done(function (msg) {
            console.log(msg); //debugging
            window.location.href = "project.php?page=sent"; //go to sent page when message sending is done
        }).always(function () {
            console.log("request finished");  //debugging
        })
    });
}

setupSendButton();
