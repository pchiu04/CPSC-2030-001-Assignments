let rifles = [
    [
        "https://csgostash.com/img/weapons/AK-47.png",
        "https://csgostash.com/img/weapons/AUG.png",
        "https://csgostash.com/img/weapons/AWP.png",
        "https://csgostash.com/img/weapons/FAMAS.png",
        "https://csgostash.com/img/weapons/G3SG1.png",
        "https://csgostash.com/img/weapons/Galil_AR.png",
        "https://csgostash.com/img/weapons/M4A1-S.png",
        "https://csgostash.com/img/weapons/M4A4.png",
        "https://csgostash.com/img/weapons/SCAR-20.png",
        "https://csgostash.com/img/weapons/SG_553.png",
        "https://csgostash.com/img/weapons/SSG_08.png"
    ],
    [
        "https://csgostash.com/img/weapons/MP5-SD.png",
        "https://csgostash.com/img/weapons/MAC-10.png",
        "https://csgostash.com/img/weapons/MP7.png",
        "https://csgostash.com/img/weapons/MP9.png",
        "https://csgostash.com/img/weapons/PP-Bizon.png",
        "https://csgostash.com/img/weapons/P90.png",
        "https://csgostash.com/img/weapons/UMP-45.png",
    ],
    [
        "https://csgostash.com/img/weapons/MAG-7.png",
        "https://csgostash.com/img/weapons/Nova.png",
        "https://csgostash.com/img/weapons/Sawed-Off.png",
        "https://csgostash.com/img/weapons/XM1014.png",
        "https://csgostash.com/img/weapons/M249.png",
        "https://csgostash.com/img/weapons/Negev.png"
    ],
    [
        "https://csgostash.com/img/weapons/CZ75-Auto.png",
        "https://csgostash.com/img/weapons/Desert_Eagle.png",
        "https://csgostash.com/img/weapons/Dual_Berettas.png",
        "https://csgostash.com/img/weapons/Five-SeveN.png",
        "https://csgostash.com/img/weapons/Glock-18.png",
        "https://csgostash.com/img/weapons/P2000.png",
        "https://csgostash.com/img/weapons/P250.png",
        "https://csgostash.com/img/weapons/R8_Revolver.png",
        "https://csgostash.com/img/weapons/Tec-9.png",
        "https://csgostash.com/img/weapons/USP-S.png"
    ],
    [
        "https://purepng.com/public/uploads/large/purepng.com-motorcycle-helmetmotorcyclehelmetmotorcycle-helmetbike-helmetmoto-helmetsafety-helmet-17015279091688kuvh.png",
        "http://pngimg.com/uploads/gloves/gloves_PNG8287.png",
        "https://www.armorexpress.com/wp-content/uploads/2018/01/Revolution-Front-View.png",
        "https://www.demonunited.com/img/DS5115hyperkneeshin-xd30.png",
        "https://www.oneal.com/wp-content/uploads/2018/06/On_rdx_blk_boot_pair_front_18_sml.png"
    ]
];

function setup() {
    let displayList = document.querySelector(".weapons");

    for (let i = 0; i < rifles[0].length; i++){
        let element = document.createElement("div");
        element.classList.add('gear');
        let img = document.createElement("img");
        img.setAttribute('src', rifles[0][i]);

        element.appendChild(img);
        displayList.appendChild(element);
    }
    return displayList;
}

setup();

function createClickHandler(index){
    return function(){
        $(".weapons").empty();
        let displayList = document.querySelector(".weapons");

        for (let i = 0; i < rifles[index].length; i++){
            let element = document.createElement("div");
            element.classList.add('gear');
            let img = document.createElement("img");
            img.setAttribute('src', rifles[index][i]);

            element.appendChild(img);
            displayList.appendChild(element);
        }
    }
}

let menu = document.querySelectorAll(".loadout>div");

for (let j = 0; j < menu.length; j++) {
    let clickH = createClickHandler(j);
    menu[j].addEventListener("click", clickH);
}
