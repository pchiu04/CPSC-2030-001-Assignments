<?php
session_start();

require_once "sqlhelper.php";
require_once './vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);


//sql setup
$user = 'root';
$pwd = '';
$server = 'localhost';
$dbname = 'forms';
$conn = new mysqli($server, $user, $pwd, $dbname);


$project_page = $twig->load('main.twig.html');

if (isset($_POST["user"]) && isset($_POST["email"]) && isset($_POST["location"]) && isset($_POST["phone"]) && isset($_POST["message"])){
    $user = $conn->escape_string($_POST["user"]);
    $email = $conn->escape_string($_POST["email"]);
    $location = $conn->escape_string($_POST["location"]);
    $phone = $conn->escape_string($_POST["phone"]);
    $message = $conn->escape_string($_POST["message"]);
    $queryString = "call insert_info('$user', '$email', '$location', '$phone', '$message')"; //SQL insert
    clearConnection($conn);
    $result = $conn->query($queryString);


    if($result){
        echo json_encode(array("success"=>"message inserted"));
    } else {
        echo json_encode(array("error"=>"unknown sql error"));
    }

}

if (isset($_GET['page'])){
    if ($_GET['page'] == 'event'){
        $project_page = $twig->load('event.html');
    } else if ($_GET['page'] == 'rules'){
        $project_page = $twig->load('rules.html');
    } else if ($_GET['page'] == 'airsoft'){
        $project_page = $twig->load('airsoft.html');
    } else if ($_GET['page'] == 'faq'){
        $project_page = $twig->load('faq.html');
    } else if ($_GET['page'] == 'contact'){
        $project_page = $twig->load('contact.html');
    } else if ($_GET['page'] == 'sent'){
        $project_page = $twig->load('messageSent.html');
    }
}


echo $project_page->render(array("title"=>"Hunt of Airsoft"));

//session_destroy();
?>
