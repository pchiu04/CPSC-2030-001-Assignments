
let names = [
    "Claude Wallace",
    "Riley Miller",
    "Raz",
    "Kai Schulen",
    "Angelica Farnaby",
    "Minerva Victor",
    "Karen Stuart",
    "Ragnarok",
    "Miles Arbeck",
    "Dan Bentley"
];

let sides = [
    "Edinburgh Army",
    "Edinburgh Army",
    "Edinburgh Army",
    "Edinburgh Army",
    "N/A",
    "Edinburgh Army",
    "Edinburgh Army",
    "Edinburgh Army",
    "Edinburgh Army",
    "Edinburgh Army"
];

let units = [
    "Ranger Corps, Squad E",
    "Federate Joint Ops",
    "Ranger Corps, Squad E",
    "Ranger Corps, Squad E",
    "N/A",
    "Ranger Corps, Squad F",
    "Squad E",
    "Squad E",
    "Ranger Corps, Squad E",
    "Ranger Corps, Squad E"
];

let ranks = [
    "First Lieutenant",
    "Second Lieutenant",
    "Sergeant",
    "Sergeant Major",
    "N/A",
    "First Lieutenant",
    "Corporal",
    "K-9 Unit",
    "Sergeant",
    "Private First Class"
];

let roles = [
    "Tank Commander",
    "Artillery Advisor",
    "Fireteam Leader",
    "Fireteam Leader",
    "N/A",
    "Senior Commander",
    "Combat EMT",
    "Mascot",
    "Tank Operator",
    "APC Operator"
];

let descriptions = [
    "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
    "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",
    "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible",
    "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename \"Deadeye Kai.\" Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.",
    "A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed \"Angie,\" she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name.",
    "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
    "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",
    "Once a stray, this good good boy is lovingly referred to as \"Rags.\"As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",
    "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
    "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat."
];

let imagesURL = [
    "http://valkyria.sega.com/img/character/chara-ss01.jpg",
    "http://valkyria.sega.com/img/character/chara-ss02.jpg",
    "http://valkyria.sega.com/img/character/chara-ss03.jpg",
    "http://valkyria.sega.com/img/character/chara-ss04.jpg",
    "http://valkyria.sega.com/img/character/chara-ss14.jpg",
    "http://valkyria.sega.com/img/character/chara-ss11.jpg",
    "http://valkyria.sega.com/img/character/chara-ss12.jpg",
    "http://valkyria.sega.com/img/character/chara-ss13.jpg",
    "http://valkyria.sega.com/img/character/chara-ss15.jpg",
    "http://valkyria.sega.com/img/character/chara-ss16.jpg"
];

//this returns all the character names under the class 'characterName'>'div'
function loadCharacter(){
    let list = document.createElement('div');
    list.className = "characterName";
    for (let i = 0; i < names.length; i++) {
        let item = document.createElement('div');
        item.appendChild(document.createTextNode(names[i]));
        list.appendChild(item);
    }
    return list;
}

//this creates event handler to each of the characters and the characters that user clicked
function createClickHandler(i, character){
    return function(){
        if (n < 5){
            //get all the characters in squadList
            let allSquadCharacters = document.querySelectorAll(".squadCharacter");
            //compare each of them with the user click
            for (let char of allSquadCharacters){
                //if the character exist, exit the function
                if (char.innerHTML == character.innerHTML){
                    return;
                }
            }

            //grab a copy of the character name that user clicked
            let clone = character.cloneNode(true);
            clone.className = "squadCharacter";

            //and set event handlers to it
            let click = createRemoveClickHandler(character);
            let hover = createHoverHandler(i);
            clone.addEventListener("click", click);
            clone.addEventListener("mouseover", hover);

            document.getElementById("squadList").appendChild(clone);
            n++; //increment squad members count
        }
    }
}

//this simply removes the character that got clicked from the squad list
function createRemoveClickHandler(character){
    return function(){
        let allSquad = document.querySelectorAll(".squadCharacter");
        for (let i = 0; i < allSquad.length; i++) {
            if (allSquad[i].innerHTML == character.innerHTML){
                allSquad[i].parentNode.removeChild(allSquad[i]);;
                n--; //decrement squad members count
            }
        }
    }
}

//this simply show information when hovering over the character
function createHoverHandler(index){
    return function(){
        let profile = document.createElement('div');
        let side = document.createElement('div');
        let unit = document.createElement('div');
        let rank = document.createElement('div');
        let role = document.createElement('div');
        let description = document.createElement('div');

        side.appendChild(document.createTextNode("Side: " + sides[index]));
        unit.appendChild(document.createTextNode("Unit: " + units[index]));
        rank.appendChild(document.createTextNode("Rank: " + ranks[index]));
        role.appendChild(document.createTextNode("Role: " + roles[index]));
        description.appendChild(document.createTextNode(descriptions[index]));

        profile.appendChild(side);
        profile.appendChild(unit);
        profile.appendChild(rank);
        profile.appendChild(role);
        profile.appendChild(description);

        document.getElementById("profile").innerHTML = profile.innerHTML;

        let img = document.createElement('img');
        img.setAttribute('src', imagesURL[index]);
        let existingImg = document.querySelector("#picture img"); //select the current existing image
        if(existingImg){ //if the image exist,
            existingImg.outerHTML=""; //remove it
        }
        document.getElementById("picture").appendChild(img);
    }
}


//code here
//this calls the loadCharacter function to load all the characters to armylist
document.getElementById("armyList").appendChild(loadCharacter());

let n = 0; //global counter that counts the number of squad members

//grab all the characters
let characters = document.querySelectorAll(".characterName>div");

//loop through all of them and set event handlers to each of them
for (let i = 0; i < characters.length; i++) {
    let clickH = createClickHandler(i, characters[i]);
    let hoverH = createHoverHandler(i);
    characters[i].addEventListener("click", clickH);
    characters[i].addEventListener("mouseover", hoverH);
}
//code end
